package IO;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import parser.Parser;
import terminos.Clausula;
import terminos.Programa;

public class DiscoIO {

	
	public static Programa leerPrograma( String ruta ) throws Exception{
		Programa programa = new Programa();
		File file = new File(ruta);
		compruebaRutaFichero(file);
		FileReader fr = new FileReader(file);
		BufferedReader br = null;
		try{
			br = new BufferedReader(fr);

			String linea;
			while( (linea=br.readLine()) != null ){
				if( ! linea.equals("") )
				{
					if( Parser.isClausula(linea) ){
						Clausula clausula = Parser.parseClausula(linea);
						programa.addRegla(clausula);
					}else{
						throw new Exception( "La línea \"" + linea + "\" no se corresponde con una cláusula válida");
					}
				}
			}
		}finally{
			br.close();
		}
		return programa;
	}

	private static void compruebaRutaFichero(File file) throws IOException {
		if( !file.exists() ){
			throw new IOException( "El fichero no existe.");
		}
	}
}
