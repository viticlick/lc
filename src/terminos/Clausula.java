package terminos;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import algoritmia.FailException;

public class Clausula {
	private Termino cabeza;
	private List<Termino> cuerpo;
	
	public Clausula(Termino cabeza, List<Termino> lista) {
		this.cabeza = cabeza;
		this.cuerpo = lista;
	}

	public Termino getCabeza() {
		return cabeza;
	}

	public List<Termino> getCuerpo() {
		return cuerpo;
	}

	public int numElementos(){
		return cuerpo.size();
	}
	
	public boolean hasElementos(){
		return cuerpo.size() > 0;
	}
	
	public boolean equals( Clausula cl ){
		boolean equals = true;
		//Comprueba igualdad de las cabezas
		if( cabeza.equals( cl.getCabeza() )){
			List<Termino> terminosCL = cl.getCuerpo();
			//Comprueba igualdad del cuerpo
			if( terminosCL.size() == cuerpo.size() ){
				int numElementos = cuerpo.size();
				for( int indice = 0; 
						indice < numElementos 
						&& equals ; 
						indice++){
					Termino term1 = cuerpo.get(indice);
					Termino term2 = terminosCL.get(indice);
					if( ! term1.equals( term2 ) ){
						equals = false;
					}
				}
			}else{
				equals = false;
			}
		}else{
			equals = false;
		}
		return equals;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(cabeza.toString());
		int elementos = cuerpo.size();
		if( elementos == 0 ){
			sb.append(".");
		}else{
			sb.append(":-");
			int indice = 1;
			char sufijo = ',';
			Iterator<Termino> iterador = cuerpo.iterator();
			while( iterador.hasNext() ){
				Termino term = iterador.next();
				sb.append(term.toString());
				if( indice == elementos ){
					sufijo = '.';
				}
				sb.append(sufijo);
				indice++;
			}
		}
		
		return sb.toString();
	}
	
	public Clausula clone(){
		LinkedList<Termino> lista = new LinkedList<>();
		for( Termino termino : cuerpo){
			lista.add( termino );
		}
		return new Clausula( cabeza , lista );
	}
	
	public Clausula sustituir( Termino termExistente , Termino termSustitutor ) throws FailException,Exception{
		Termino cabeza = this.cabeza.sustituir(termExistente, termSustitutor);
		List<Termino> cuerpo = new LinkedList<Termino>();
		for (Termino termino : this.cuerpo) {
			Termino nuevoTermino = termino.sustituir(termExistente, termSustitutor);
			cuerpo.add(nuevoTermino);
		}
		Clausula clausula = new Clausula(cabeza, cuerpo);
		return clausula;
	}
	
	public ListaVariables getListaVariables(){
		ListaVariables lista = new ListaVariables();
		Compuesto cabeza = (Compuesto) this.cabeza;
		lista.insertaVariable( cabeza.getListaVariables() );
		Iterator<Termino> iterador = cuerpo.iterator();
		while( iterador.hasNext() ){
			Termino termino = iterador.next();
			if( termino.tipo == TipoTermino.COMPUESTO){
				Compuesto compuesto = (Compuesto) termino;
				ListaVariables variablesNuevas = compuesto.getListaVariables();
				lista.insertaVariable( variablesNuevas );
			}
		}
		return lista;
	}
	
	public boolean hasVariables(){
		return ! this.getListaVariables().getLista().isEmpty();
	}
	
	public boolean elementoSimple(){
		return ! hasVariables() && this.numElementos() == 0; 
	}
	
}
