package terminos;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Solucion {

	List<ParDeTerminos> solucion = new LinkedList<ParDeTerminos>();
	
	public Solucion ( List<ParDeTerminos> solucion ){
		this.solucion = solucion;
	}
	
	public Solucion(){}
	
	
	public List<ParDeTerminos> getPares(){
		return solucion;
	}
	
	public boolean equals( Solucion comparacion ) throws Exception{
		boolean iguales = true;
		List<ParDeTerminos> solucionAux = comparacion.getPares();
		if( solucion.size() == solucionAux.size() ){
			Iterator<ParDeTerminos> it1 = solucion.iterator();
			Iterator<ParDeTerminos> it2 = solucionAux.iterator();
			while( iguales 
					&& it1.hasNext() ){
				ParDeTerminos par1 = it1.next();
				ParDeTerminos par2 = it2.next();
				iguales = par1.equals(par2);
			}
		}else{
			iguales = false;
		}
		return iguales;
	}	
	
	public Solucion clone(){
		Solucion clone;
		List<ParDeTerminos> lista = new LinkedList<>();
		for( ParDeTerminos par : solucion){
			ParDeTerminos parAux = par.clone();
			lista.add(parAux);
		}
		clone = new Solucion(lista);
		return clone;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		if( solucion.size() > 0 ){
			Iterator<ParDeTerminos> iterador = solucion.iterator();
			while( iterador.hasNext() ){
				ParDeTerminos par = iterador.next();
				if( par.hasVariables() ){
					try {
						sb.append( par.getVariable() );
						sb.append(" = ");
						sb.append( par.getTermino() );
						sb.append("\n");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}else{
			sb.append("{}");
		}
		return sb.toString();
	}

	public Solucion aplicaTransitividad(Solucion solucioParcial) throws Exception {
		Solucion solucionTransitiva;
		if( this.solucion.size() == 0){
			solucionTransitiva = solucioParcial;
		}else{
			solucionTransitiva = this.clone();
			for (ParDeTerminos par : solucioParcial.getPares() ) {
				solucionTransitiva = solucionTransitiva.aplicaTransitividad(par);
			}
		}
		return solucionTransitiva;
	}
	
	private Solucion aplicaTransitividad( ParDeTerminos parSustitucion ) throws Exception{
		Solucion clone;
		List<ParDeTerminos> lista = new LinkedList<>();
		Termino variableSustitucion = parSustitucion.getVariable();
		Termino terminoSustitucion = parSustitucion.getTermino();
		for( ParDeTerminos par : solucion){
			if( par.getTermino().equals(variableSustitucion)  ){
				ParDeTerminos parAux = new ParDeTerminos(par.getVariable(), terminoSustitucion );
				lista.add(parAux);
			}else{
				ParDeTerminos parAux = par.clone();
				lista.add(parAux);
			}
		}
		clone = new Solucion(lista);
		return clone;
	}

	public boolean contieneVariable(Termino variable) throws Exception {
		boolean contiene = false;
		Iterator<ParDeTerminos> iterador = solucion.iterator();
		while( !contiene 
				&& iterador.hasNext() ){
			ParDeTerminos par = iterador.next();
			Termino aux = par.getVariable();
			contiene = aux.equals(variable);
		}
		return contiene;
	}

	public boolean contieneTermino(Caracter variable) {
		boolean contiene = false;
		Iterator<ParDeTerminos> iterador = solucion.iterator();
		while( !contiene 
				&& iterador.hasNext() ){
			ParDeTerminos par = iterador.next();
			Termino aux = par.getTermino();
			contiene = aux.equals(variable);
		}
		
		return contiene;
	}
	

}