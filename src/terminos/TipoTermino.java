/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package terminos;

/**
 *
 * @author vlopez
 */
public enum TipoTermino {
	CORTE,
    NUMERICO,
    CADENA,
    CARACTER,
    VARIABLE,
    COMPUESTO, 
    FAIL
}
