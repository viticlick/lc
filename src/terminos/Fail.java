package terminos;

import algoritmia.FailException;

public class Fail extends Termino {

	TipoTermino tipo = TipoTermino.FAIL;
	
	@Override
	public Object getValor() {
		return null;
	}

	
	public TipoTermino getTermino(){
		return this.tipo;
	}
	
	@Override
	public boolean equals(Termino termino) {
		return termino.getTipo() == TipoTermino.FAIL ;
	}

	@Override
	public ParDeTerminos getDiscordancia(Termino termino) {
		ParDeTerminos discordancia = null;
		if( !this.equals(termino)){
            discordancia = new ParDeTerminos( this , termino );
        }
        return discordancia;
	}

	@Override
	public boolean contieneTermino(Termino termino) {
		return false;
	}

	@Override
	public Termino sustituir(Termino termExistente, Termino termSustitutor)
			throws FailException, Exception {
		return this;
	}

	@Override
	public String toString() {
		return "fail";
	}

}
