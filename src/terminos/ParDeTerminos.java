/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package terminos;

import algoritmia.FailException;

/**
 *
 * @author vlopez
 */
public class ParDeTerminos {
    boolean hasVariable;
    private Termino variable; 
    private Termino termino; 
    private Termino primero;
    private Termino segundo;

    public ParDeTerminos(Termino primero, Termino segundo) {
        this.primero = primero;
        this.segundo = segundo;
        
        if( primero.getTipo() == TipoTermino.VARIABLE){
            this.variable = primero;
            this.termino = segundo;
            hasVariable = true;
        }else if( segundo.getTipo() == TipoTermino.VARIABLE){
            this.variable = segundo;
            this.termino = primero;
            hasVariable = true;
        }else{
            hasVariable = false;
        }
    }
    
    public Termino getPrimero(){
        return primero;
    }
    
    public Termino getSegundo(){
        return segundo;
    }

    public Termino getVariable() throws FailException {
        if( !hasVariable ){
            throw new FailException( "No existe ninguna variable en el par de t��rminos");
        }
        return variable;
    }

    public Termino getTermino() {
        return termino;
    }
    
    public boolean hasVariables(){
        return hasVariable;
    }
    
        public boolean equals( ParDeTerminos par) throws Exception{
        return( this.primero.equals(par.getPrimero())
                && this.segundo.equals(par.getSegundo())
                &&(
                    !this.hasVariable 
                || this.variable.equals(par.getVariable())));
    }
        
    
    public ParDeTerminos clone(){
    	return new ParDeTerminos(this.primero, this.segundo);
    }
        
        
    public String toString(){
    	StringBuilder sb = new StringBuilder();
    	sb.append("{");
    	if( this.hasVariables() ){
    		sb.append(this.variable.toString());
    		sb.append("/");
    		sb.append(this.termino.toString());
    	}else{
    		sb.append( this.primero.toString() );
    		sb.append("/");
    		sb.append( this.segundo.toString() );
    	}
    	sb.append("}");
    	return sb.toString();
    }
        
}
