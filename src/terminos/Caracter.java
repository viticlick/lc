/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package terminos;

/**
 *
 * @author vlopez
 */
public class Caracter extends Termino{
    
    private char valor;
    
    public Caracter( char valor ){
        this.valor = valor;
        this.tipo = TipoTermino.CARACTER;
    }

    @Override
    public Object getValor() {
        return this.valor;
    }

    @Override
    public boolean equals(Termino termino) {
        boolean iguales = false;
        if( termino.getTipo() == this.tipo){
            if( this.valor == (char) termino.getValor()){
                iguales = true;
            }
        }
        return iguales;
    }

    @Override
    public ParDeTerminos getDiscordancia(Termino termino) {
        ParDeTerminos discordancia = null;
        if( !this.equals(termino)){
            discordancia = new ParDeTerminos( this , termino );
        }
        return discordancia;
    }

    @Override
    public boolean contieneTermino(Termino termino) {
        return false;
    }
    
    @Override
    public Termino sustituir(Termino termExistente, Termino termSustitutor) {
        return this;
    }

    @Override
    public String toString() {
        return  String.valueOf(valor);
    }
}
