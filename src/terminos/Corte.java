package terminos;

import algoritmia.FailException;

public class Corte extends Termino {

	TipoTermino tipo = TipoTermino.CORTE;
	
	public TipoTermino getTipo(){
		return this.tipo;
	}
	
	@Override
	public Object getValor() {
		return null;
	}

	@Override
	public boolean equals(Termino termino) {
		return ( termino.getTipo() == this.tipo );
	}

	@Override
	public ParDeTerminos getDiscordancia(Termino termino) {
		ParDeTerminos discordancia = null;
        if( !this.equals(termino)){
            discordancia = new ParDeTerminos( this , termino );
        }
        return discordancia;
	}

	@Override
	public boolean contieneTermino(Termino termino) {
		return false;
	}

	@Override
	public Termino sustituir(Termino termExistente, Termino termSustitutor)
			throws FailException {
		return this;
	}

	@Override
	public String toString() {
		return "!";
	}

}
