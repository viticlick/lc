/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package terminos;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author vlopez
 */
public class Compuesto extends Termino{

    private List<Termino> listaTerminos;
    private String nombreFuncion;
    private int numValores;
    
    public Compuesto( String nombre , List<Termino> terminos ) throws Exception{
        this.tipo = TipoTermino.COMPUESTO;
        this.nombreFuncion = nombre.trim();
        this.listaTerminos = terminos;
        this.numValores = listaTerminos.size();
        if( numValores == 0 ){
        	throw new Exception( "No se puede crear un tipo compuesto sin contenido");
        }
    }
    
    public List<Termino> getTerminos(){
        return listaTerminos;
    }
    
    @Override
    public Object getValor() {
        String firma = nombreFuncion + String.valueOf(numValores);
        return firma;
    }

    @Override
    public boolean equals(Termino termino) {
        boolean iguales = false;
        if( termino.getTipo() == this.tipo
                && this.getValor().equals(termino.getValor())){
            List<Termino> listaTerminosAux = ( (Compuesto) termino ).getTerminos();
            iguales = comparaListaTerminos(  listaTerminosAux );
        }
        return iguales;
    }

    private boolean comparaListaTerminos(List<Termino> listaTerminosAux) {
        boolean iguales = true;
        if( numValores == listaTerminosAux.size() ){
            for( int indice = 0 ; indice < numValores ; indice++ ){
                Termino terminoPropio = listaTerminos.get(indice);
                Termino terminoExterno = listaTerminosAux.get(indice);
                if( !terminoPropio.equals(terminoExterno) ){
                    iguales = false;
                    break;
                }
            }
        }else{
            iguales = false;
        }
        return iguales;
    }

    @Override
    public ParDeTerminos getDiscordancia(Termino termino) {
        ParDeTerminos discordancia = null;
        if( this.tipo != termino.getTipo() ){
            discordancia = new ParDeTerminos( this , termino);
        }
        else{
            String firmaPropia = (String) this.getValor();
            String firmaExterno = (String) termino.getValor();
            if( firmaPropia.equals(firmaExterno)){
                List<Termino> listaExterna = ((Compuesto) termino).getTerminos();
                discordancia = getPrimeraDiscordancia( listaExterna );
            }    
        }
        return discordancia;
    }

    private ParDeTerminos getPrimeraDiscordancia(List<Termino> listaExterna) {
        
        ParDeTerminos discordancia = null;
        int indice = 0;
        while( indice < numValores 
                && discordancia == null)
        {
            Termino valorPropio = listaTerminos.get(indice);
            Termino valorExterno = listaExterna.get(indice);
            discordancia = valorPropio.getDiscordancia(valorExterno);
            indice++;
        }

        return discordancia;
    }
    
    public boolean contieneTermino( Termino termino ){
        boolean occurence = false;
        int indice = 0;
        while( indice < numValores 
                && !occurence)
        {
            Termino terminoLista = listaTerminos.get(indice);
            occurence = (
                    terminoLista.equals(termino)
                    || terminoLista.contieneTermino(termino));
            indice++;
        }
        return occurence;
    }
    
    @Override
    public Termino sustituir( Termino termExistente , Termino termSustitutor ) throws Exception{
        
        LinkedList<Termino> lista = new LinkedList<>();
        Iterator<Termino> it = this.listaTerminos.iterator();
        while( it.hasNext() ){
            Termino t = it.next();
            if( t.equals(termExistente) ){
                lista.add(termSustitutor);
            }
            else{
                Termino t2 = t.sustituir(termExistente, termSustitutor);
                lista.add(t2);
            }
        }
        
        Compuesto toret = new Compuesto( this.nombreFuncion , lista );
        return toret;
    }
  
    public ListaVariables getListaVariables(){
    	ListaVariables lista = new ListaVariables();
    	Iterator<Termino> iterador = this.listaTerminos.iterator();
    	while( iterador.hasNext() ){
    		Termino termino = iterador.next();
    		if( termino.getTipo() == TipoTermino.VARIABLE )
    		{
    			lista.insertaVariable((Variable) termino ); 
    		}else if( termino.getTipo() == TipoTermino.COMPUESTO ){
    			Compuesto compuesto = (Compuesto) termino;
    			ListaVariables listaAux = compuesto.getListaVariables();
    			lista.insertaVariable(listaAux);
    		}
    	}
    	return lista;
    }
    
    public boolean hasVariables(){
    	return ! this.getListaVariables().getLista().isEmpty();
    }
    
    
    
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(nombreFuncion);
        sb.append("(");
        
        Iterator<Termino> it = listaTerminos.iterator();
        while( it.hasNext() ){
            Termino term = it.next();
            sb.append( term.toString() );
            sb.append(",");
        }
        
        // elimina la ��ltima coma
        sb.deleteCharAt(sb.length()-1);
        
        sb.append(")");
        return sb.toString();
    }
}
