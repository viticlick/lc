/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package terminos;

import algoritmia.FailException;

/**
 *
 * @author vlopez
 */
public abstract class Termino {
    
    protected TipoTermino tipo;
    
    public TipoTermino getTipo(){
        return tipo;
    }
    
    public abstract Object getValor();
    public abstract boolean equals( Termino termino );
    public abstract ParDeTerminos getDiscordancia(Termino termino);
    public abstract boolean contieneTermino( Termino termino );
    public abstract Termino sustituir( Termino termExistente , Termino termSustitutor) throws FailException, Exception;
    public abstract String toString();
}
