/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package terminos;

/**
 *
 * @author vlopez
 */
public class Numerico extends Termino{
    
    double valor;
    
    public Numerico( double valor){
        this.valor = valor;
    }

    @Override
    public Object getValor() {
        return valor;
    }

    @Override
    public boolean equals(Termino termino) {
        boolean iguales = false;
        if( termino.getTipo() == this.tipo ){
            if( this.valor == (double) termino.getValor() ){
                iguales = true;
            }
        }
        return iguales;
    }

    @Override
    public ParDeTerminos getDiscordancia(Termino termino) {
        ParDeTerminos discordancia = null;
        if( !this.equals(termino)){
            discordancia = new ParDeTerminos( this , termino );
        }
        return discordancia;
    }

    @Override
    public boolean contieneTermino(Termino termino) {
        return false;    
    }
    
    
    @Override
    public Termino sustituir(Termino termExistente, Termino termSustitutor) {
        return this;
    }

    @Override
    public String toString() {
        return Long.toString(Math.round(valor));
    }

}
