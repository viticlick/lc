package terminos;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Programa {

	List<Clausula> reglasPrograma;
	
	public Programa(){
		reglasPrograma = new LinkedList<Clausula>();
	}
	
	public Programa( List<Clausula> programa){
		reglasPrograma = programa;
	}
	
	public void addRegla( Clausula clausula ){
		reglasPrograma.add(clausula);
	}
	
	public List<Clausula> getReglasPrograma(){
		return reglasPrograma;
	}
	
	public Iterator<Clausula> iterator(){
		return reglasPrograma.iterator();
	}
	
	public int getNumeroReglas(){
		return reglasPrograma.size();
	}

	public Clausula getReglaAt(int indice) {	
		return reglasPrograma.get(indice);
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		for( Clausula cl : reglasPrograma){
			sb.append( cl.toString() );
			sb.append( System.getProperty("line.separator"));
		}
		return sb.toString();
	}
 	
}
