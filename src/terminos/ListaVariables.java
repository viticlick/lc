package terminos;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ListaVariables {

	private List<Variable> lista = new LinkedList<Variable>();
	
    public List<Variable> getLista() {
		return lista;
	}

	public void insertaVariable( Variable var ){
		if( lista.isEmpty() ){
			lista.add( var );
		}else{
			boolean contieneElemento  = false;
			Iterator<Variable> iterador = lista.iterator();
			while( !contieneElemento
					&& iterador.hasNext() )
			{
				Termino termino = iterador.next();
				if( termino.equals(var)){
					contieneElemento = true;
				}
			}
			if( ! contieneElemento ){
				lista.add( var );
			}
		}
    }
    
    public void insertaVariable(  List<Variable> variablesNuevas )
    {
    	Iterator<Variable> iterator = variablesNuevas.iterator();
    	while( iterator.hasNext() ){
    		Variable variable = iterator.next();
    		insertaVariable(variable);
    	}
    }

	public void insertaVariable(ListaVariables lista) {
		List<Variable> nuevaLista = lista.getLista();
		insertaVariable(nuevaLista);
	}
}
