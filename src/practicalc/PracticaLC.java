/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package practicalc;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import parser.Parser;
import algoritmia.SLD;
import terminos.Clausula;
import terminos.Compuesto;
import terminos.Programa;
import terminos.Termino;

/**
 *
 * @author vlopez
 */
public class PracticaLC {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    	
    	Pattern consultPattern = Pattern.compile("^consult[(].*[)]$");
    	
		try {	
			
			System.out.println("Cargue el programa con el comando 'consult(<rutaPrograma>)'");
			System.out.print("? ");
			BufferedReader br = new BufferedReader( new InputStreamReader(System.in));
			boolean finPrograma = false;
			boolean programaCargado = false;
			boolean preguntaHecha = false;
			SLD sld = null;
			while( ! programaCargado ){
				String ruta = br.readLine();
				Matcher matcher = consultPattern.matcher(ruta);
				if( matcher.matches() ){
					try{
						ruta = ruta.substring(8 ,ruta.length() - 1);
						Programa programa = IO.DiscoIO.leerPrograma(ruta);
						sld = new SLD(programa);
						programaCargado = true;
						System.out.println("yes");
					}catch( Exception ex){
						System.out.println( ex.getMessage() );
					}
				}else if( ruta.equals("halt")){
					finPrograma = true;
				}else{
					System.out.println("Cargue el programa con el comando 'consult(<rutaPrograma>)' o 'halt' para salir del programa");
				}
			}
			while( ! finPrograma ){
				System.out.print("? ");
				String linea = br.readLine();
				Matcher matcher = consultPattern.matcher(linea);
				if( matcher.matches() ){
					try{
						linea = linea.substring(8 , linea.length() - 1);
						Programa programa = IO.DiscoIO.leerPrograma(linea);
						sld = new SLD(programa);
						System.out.println("yes");
					}catch( Exception ex){
						System.out.println( ex.getMessage() );
					}
				}else if( Parser.isCompuesto(linea) ){
					Compuesto pregunta = (Compuesto) Parser.parseTermino(linea);
					preguntaHecha = true;
					if( sld.estudiarSolucion(pregunta)){
						System.out.println( sld.getSolucion().toString() );
						System.out.println( "yes" );
					}else{
						System.out.println( "no" );
					}
				}else if( preguntaHecha && linea.equals(";")){
					if( sld.hayMasSoluciones() ) {
						System.out.println( sld.getSolucion().toString() );
						System.out.println("yes");
					}else{
						System.out.println("no");
					}
				}else if( linea.equals("halt")){
					finPrograma = true;
				}

			}
			
		} catch (Exception exception) {
			System.out.println(exception.getMessage());
		}

    }
    
}
