package test;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import terminos.Caracter;
import terminos.ParDeTerminos;
import terminos.Solucion;
import terminos.Variable;

public class TestSolucion {

	Solucion solucion1; // {X/Y}
	Solucion solucion2; // {Y/a}
	Solucion solucion3; // {X/a}
	Solucion solucion4; // {X/a,Y/b}
	Solucion solucion5; // {Y/b,X/a}
	
	Caracter charA = new Caracter('a');
    Caracter charB = new Caracter('b');
    Caracter charC = new Caracter('c');

    Variable varX = new Variable("X");
    Variable varY = new Variable("Y");
    Variable varZ = new Variable("Z");
    Variable varT = new Variable("T");
	
	public TestSolucion(){
		// SOLUCION1: {X/Y}
		List<ParDeTerminos> lista= new LinkedList<>();
		ParDeTerminos par = new ParDeTerminos( varX , varY);
		lista.add(par);
		solucion1 = new Solucion(lista);
		
		// SOLUCION2: {Y/a}
		ParDeTerminos par1 = new ParDeTerminos(varY, charA);
		LinkedList<ParDeTerminos> lista1 = new LinkedList<>();
		lista1.add(par1);
		solucion2 = new Solucion(lista1);
		
		// SOLUCION3: {X/a}
		ParDeTerminos par2 = new ParDeTerminos(varX, charA);
		LinkedList<ParDeTerminos> lista2 = new LinkedList<>();
		lista2.add(par2);
		solucion3 = new Solucion(lista2);
		
		// SOLUCION4: {X/a,Y/b}
		ParDeTerminos par4 = new ParDeTerminos(varX, charA);
		ParDeTerminos par42 = new ParDeTerminos(varY, charB);
		LinkedList<ParDeTerminos> lista4 = new LinkedList<>();
		lista4.add(par4);
		lista4.add(par42);
		solucion4 = new Solucion(lista4);
		
		// SOLUCION5: {Y/b,X/a}
		ParDeTerminos par5 = new ParDeTerminos(varX, charA);
		ParDeTerminos par52 = new ParDeTerminos(varY, charB);
		LinkedList<ParDeTerminos> lista5 = new LinkedList<>();
		lista5.add(par52);
		lista5.add(par5);
		solucion5 = new Solucion(lista5);
		
		
	}
	
	@Test
	public void testTransitividad() throws Exception {

		// SOLUCION: {X/a}
		Solucion solucion  = solucion1.aplicaTransitividad( solucion2 );
		
		assertTrue( solucion3.equals(solucion));
	}
	
	@Test
	public void testEquals() throws Exception{	
		assertTrue( solucion4.equals(solucion5) );
	}

	@Test
	public void testContieneVariable() throws Exception{
		assertTrue( solucion4.contieneVariable(varY));
	}
	
	@Test
	public void testContieneTermino(){
		assertTrue( solucion4.contieneTermino(charB));
	}
	
}
