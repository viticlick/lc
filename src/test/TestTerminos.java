package test;
import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import terminos.Cadena;
import terminos.Caracter;
import terminos.Compuesto;
import terminos.Numerico;
import terminos.ParDeTerminos;
import terminos.Termino;
import terminos.Variable;


public class TestTerminos {

	Variable variableX = new Variable("X");
    Variable variableY = new Variable("Y");
    Numerico numericoPositivo = new Numerico(12);
    Numerico numericoNegativo = new Numerico(-23);
    String cadena1 = "cadena 1";
    String cadena2 = "cadena 2";
    Character carA = new Character( 'a' );
    Character carB = new Character( 'b' );
    LinkedList<Termino> lista1 = new LinkedList<>();
    LinkedList<Termino> lista2 = new LinkedList<>();
    LinkedList<Termino> lista3 = new LinkedList<>();
    LinkedList<Termino> lista4 = new LinkedList<>();
    LinkedList<Termino> lista5 = new LinkedList<>();
    LinkedList<Termino> lista6 = new LinkedList<>();
    LinkedList<Termino> lista7 = new LinkedList<>();
    Compuesto com1;
    Compuesto com1a;
    Compuesto com1b;
    Compuesto com2;
    Compuesto com3;
    Compuesto com4;
    
    
    
    public TestTerminos() throws Exception {
        lista1.add(numericoPositivo);
        lista1.add(variableX);
        
        lista2.add(numericoPositivo);
        lista2.add(variableY);
        
        lista3.add(numericoPositivo);
        lista3.add(numericoNegativo);
        
        lista7.add(numericoNegativo);
        lista7.add(variableY);
        lista7.add(com1);
        
        
        com1 = new Compuesto("com1", lista1);       // com1(12,X)
        com1a = new Compuesto("com1",lista2);       // com1(12,Y)
        com1b = new Compuesto( "com1" , lista3 );    // com1(12,-23)
        com2 = new Compuesto("com2" , lista2);      // com2(12,Y)
        com4 = new Compuesto( "com4" , lista7 );    // com4( -23, Y , com1( 12 , X ) )
    }
    
    
    @Test
    public void testVariablesIguales(){
        assertTrue("Comprobando si las variables X y X son iguales", variableX.equals(variableX));
    }
    
    @Test
    public void testVariablesDesiguales(){
        assertFalse("Comprobando si las variables X y Y son desigules" , variableX.equals(variableY));
    }

    @Test
    public void testNumericosDesiguales(){
        assertFalse("Comprobando si las variables 12 y -23 son desigules" , numericoPositivo.equals(numericoNegativo));
    }
    
    @Test
    public void testNumericosIguales(){
        assertTrue("Comprobando si las variables 12 y 12 son desigules" , numericoPositivo.equals(numericoPositivo));
    }
    
    @Test
    public void testCadenasIguales(){
        Cadena a = new Cadena( "Esta es la cadena a");
        Cadena b = new Cadena( "Esta es la cadena a");
        assertTrue("Comprobando si las cadenas iguales retornan true", a.equals(b));
    }
    
    @Test
    public void testCadenasDesiguales(){
        assertFalse("Comprobando si las cadenas desiguales retornan false", cadena1.equals(cadena2));
    }
    
    public void testaracteresIguales(){
        Caracter a = new Caracter( 'a' );
        Caracter b = new Caracter( 'a' );
        assertTrue("Comprobando si los caracteres iguales retornan true", a.equals(b));
    }
    
    
     @Test
    public void testCaracteresDesigualesales(){
        Caracter a = new Caracter( 'a' );
        Caracter b = new Caracter ( 'b' );
        assertFalse("Comprobando si los caracteres desiguales retornan false", a.equals(b));
    }
    
    @Test
    public void testCompuestosIguales() throws Exception{
        Caracter a = new Caracter('a');
        Numerico num = new Numerico(12);
        Variable z = new Variable("Z");
        List<Termino> lista = new LinkedList<>();
        lista.add(a);
        lista.add(num);
        lista.add(z);
        
        
        Compuesto com1 = new Compuesto( "com" , lista );
        Compuesto com2 = new Compuesto( "com" , lista );
        assertTrue("Comparando dos terminos compuestos iguales", com1.equals(com2));
    }
    
    @Test
    public void testCompuestosDesiguales() throws Exception{
        Caracter a = new Caracter('a');
        Numerico num = new Numerico(12);
        Variable z = new Variable("Z");
        List<Termino> lista = new LinkedList<>();
        lista.add(a);
        lista.add(num);
        lista.add(z);
        
        List<Termino> lista2 = new LinkedList<>();
        lista2.add(a);
        lista2.add(num);
        
        List<Termino> lista3 = new LinkedList<>();
        lista3.add(a);
        lista3.add(num);
        lista3.add(num); 
        
        Compuesto com1 = new Compuesto( "com" , lista );
        Compuesto com2 = new Compuesto( "com2" , lista );
        Compuesto com3 = new Compuesto( "com" , lista2);
        Compuesto com4 = new Compuesto( "com" , lista3);
        assertFalse("Comparando dos terminos compuestos con distinto nombre", com1.equals(com2));
        assertFalse("Comparando dos terminos con distinto numero de elementos" , com1.equals(com3));
        assertFalse("Comparando dos terminos con distintos elementos", com1.equals(com4));
    }
    
    @Test
    public void testDiferentesTipos(){
        Variable variable = new Variable( "A" );
        Numerico numerico = new Numerico( -32 );
        Cadena cadena = new Cadena( "Esto es un texto");
        Caracter caracter = new Caracter('a');
        assertFalse("Comprobando si las variables Variable y Numerico" , variable.equals(numerico));
        assertFalse("Comprobando igualdad entre Variable y Cadena", variable.equals(cadena));
        assertFalse("Comprobando igualdad entre Cadena y Numerico", cadena.equals(numerico));
        assertFalse("Comprobando igualdad entre Numerico y Variable", numerico.equals(variable));
        assertFalse("Comprobando igualdad entre Cadena y Variable", numerico.equals(variable));
        assertFalse("Comprobando igualdad entre Caracter y Variable", caracter.equals(variable));
        assertFalse("Comprobando igualdad entre Caracter y Numerico", caracter.equals(numerico));
        assertFalse("Comprobando igualdad entre Caracter y Cadena", caracter.equals(cadena));
    }
    
    
    @Test
    public void testOccurCheckVariableIncluida(){
       boolean value = com1.contieneTermino(variableX);
       assertTrue("La función \"com1(X)\" contiene la variable X" , value );
    }
    
    @Test
    public void testSustitucion() throws Exception{
        Termino aux = com1.sustituir(variableX, variableY);
        assertTrue("Sustitucion com1(12,X) por {X,Y} -> resultado com1(12,Y)", com1a.equals(aux));
    }
    
    @Test
    public void testGetDiscordancia(){
        try{
            ParDeTerminos par = com1.getDiscordancia(com1b);
            ParDeTerminos par2 = new ParDeTerminos( numericoNegativo, variableX);
            ParDeTerminos par3 = new ParDeTerminos( variableX , numericoNegativo);
            boolean comparacion = par.equals(par2) || par.equals(par3);
            assertTrue("Discordancia entre com1(12,X) y com1(12,-23) -> par( X , -23 )", comparacion);
        }catch(Exception ex){
            
        }
    }

}
