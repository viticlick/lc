package test;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Test;

import parser.Parser;
import terminos.Clausula;
import terminos.Compuesto;
import terminos.Termino;
import terminos.Variable;

public class TestClausula {

	String funcion1 = "g";
	Termino variableX = new Variable("X");
	Termino variableY = new Variable("Y");
	
	@Test
	public void testEqualsSoloCabezas() throws Exception {
		LinkedList<Termino> lista1 = new LinkedList<>();
		lista1.add(variableX);
		lista1.add(variableY);
		Termino term1 = new Compuesto( funcion1 , lista1 );
		Termino term2 = new Compuesto( "g" , lista1);
		Clausula cl1 = new Clausula(term1 , new LinkedList<Termino>());
		Clausula cl2 = new Clausula(term2 , new LinkedList<Termino>());
		assertTrue( cl1.equals(cl2));
		
	}
	
	@Test
	public void testEquals() throws Exception{
		String st1 = "nieto(X,Y):-hijo(12,Y),! ,hijo(Y,adfa).";
		Clausula cl1 = Parser.parseClausula(st1);
		Clausula cl2 = Parser.parseClausula(st1);
		assertTrue( cl1.equals(cl2));
	}
	

}
