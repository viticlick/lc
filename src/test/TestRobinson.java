package test;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Test;

import algoritmia.Robinson;
import terminos.Caracter;
import terminos.Compuesto;
import terminos.Termino;
import terminos.Variable;

public class TestRobinson {

    Caracter charA = new Caracter('a');
    Caracter charB = new Caracter('b');
    Caracter charC = new Caracter('c');

    Variable varX = new Variable("X");
    Variable varY = new Variable("Y");
    Variable varZ = new Variable("Z");
    Variable varT = new Variable("T");
    
    Compuesto ejemplo10E;
    Compuesto ejemplo10F;
    
    Compuesto ejemplo13E;
    Compuesto ejemplo13F;
    
    Compuesto ejemplo14E;
    Compuesto ejemplo14F;

    Compuesto ejemplo15E; // p(a,X,h(g(Z)))
    Compuesto ejemplo15F; // p(Z,h(Y),h(Y))
    
    
    public TestRobinson() throws Exception {
        InitializeEjemplo10();
        InicializaEjemplo13();
        InicializaEjemplo14();
        InicializaEjemplo15();
    }
   

    @Test
    public void testRobinsonEjemplo13() throws Exception{
        Robinson.getUnificador(ejemplo13E, ejemplo13F);
//        Robinson.getUnificador(ejemplo14E, ejemplo14F); //Devuelve Fail
        Robinson.getUnificador(ejemplo15E, ejemplo15F);
        assertTrue(true); 
    }
    
    private void InitializeEjemplo10() throws Exception{

        LinkedList<Termino> lista10a1 = new LinkedList<>();
        LinkedList<Termino> lista10a2 = new LinkedList<>();
        LinkedList<Termino> lista10a3 = new LinkedList<>();
        LinkedList<Termino> lista10b1 = new LinkedList<>();
        LinkedList<Termino> lista10b2 = new LinkedList<>();
        LinkedList<Termino> lista10b3 = new LinkedList<>();
        LinkedList<Termino> lista10b4 = new LinkedList<>();

        lista10a3.add(varX);
        Compuesto com1a = new Compuesto("g", lista10a3); // g(X)
        
        
        lista10a2.add(charB);
        lista10a2.addLast(charC);
        Compuesto com1a2 = new Compuesto( "f" , lista10a2 ); // f(b,c)
        
        lista10a1.addFirst(charA);
        lista10a1.addLast(com1a2);
        lista10a1.addLast(com1a);
        ejemplo10E = new Compuesto("p", lista10a1); // p(a,f(b,c) , g(X))
        
        
        lista10b4.add(varZ);
        Compuesto com1b = new Compuesto("h", lista10b4); // h(Z)
        
        lista10b3.add(com1b);
        Compuesto com1b2 = new Compuesto("g", lista10b3); // g(h(Z))
        
        lista10b2.add(varX);
        lista10b2.addLast(varY);
        Compuesto com1b3 = new Compuesto("f", lista10b2); // f(X,Y)
        
        lista10b1.add(charA);
        lista10b1.addLast(com1b3);
        lista10b1.addLast(com1b2);
        ejemplo10F = new Compuesto("p", lista10b1); // p(a,f(X,Y),g(h(Z)))
        
    }
    
    private void InicializaEjemplo13() throws Exception{
        LinkedList<Termino> listaA  = new LinkedList<>();
        listaA.add(charA);
        listaA.addLast(varX);
        ejemplo13E = new Compuesto("p", listaA); // p(a,X)
        
        LinkedList<Termino> listaB  = new LinkedList<>();
        listaB.add(varX);
        listaB.addLast(varY);
        ejemplo13F = new Compuesto("p", listaB); //p(X,Y)
    }
    
    private void InicializaEjemplo14() throws Exception{
        
        LinkedList<Termino> listaE1 = new LinkedList<>();
        listaE1.add(varX);
        listaE1.addLast(charB);
        Compuesto E1 = new Compuesto("f", listaE1);
        
        LinkedList<Termino> listaE = new LinkedList<>();
        listaE.add(charA);
        listaE.add(E1);
        listaE.add(varY);
        
        ejemplo14E = new Compuesto("p", listaE);
        
        LinkedList<Termino> listaF1 = new LinkedList<>();
        listaF1.add(varY);
        Compuesto F1 = new Compuesto("g", listaF1);
        LinkedList<Termino> listaF2 = new LinkedList<>();
        listaF2.add(F1);
        listaF2.add(varZ);
        Compuesto F2 = new Compuesto("f", listaF2);
        
        LinkedList<Termino> listaF = new LinkedList<>();
        listaF.add(varX);
        listaF.add(F2);
        listaF.add(varT);
        
        ejemplo14F = new Compuesto("p", listaF);
    }
    
    private void InicializaEjemplo15() throws Exception{
        LinkedList<Termino> lista1 = new LinkedList<>();
        lista1.add(varZ);
        Compuesto E1 = new Compuesto("g",lista1);
        
        LinkedList<Termino> lista2 = new LinkedList<>();
        lista2.add(E1);
        Compuesto E2 = new Compuesto("h",lista2);
        
        LinkedList<Termino> listaE = new LinkedList<>();
        listaE.add(charA);
        listaE.add(varX);
        listaE.add(E2);
        ejemplo15E = new Compuesto( "p", listaE); // p(a,X,h(g(Z)))
        
        LinkedList<Termino> lista3 = new LinkedList<>();
        lista3.add(varY);
        Compuesto F1 = new Compuesto("h", lista3);
        LinkedList<Termino> lista4 = new LinkedList<>();
        lista4.add(varY);
        Compuesto F2 = new Compuesto("h", lista4);
        
        LinkedList<Termino> listaF = new LinkedList<>();
        listaF.add(varZ);
        listaF.add(F1);
        listaF.add(F2);
        ejemplo15F = new Compuesto("p", listaF); // p(Z,h(Y),h(Y))
    }
}
