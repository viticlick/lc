package test;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import algoritmia.SLD;
import parser.Parser;
import terminos.Caracter;
import terminos.Clausula;
import terminos.Compuesto;
import terminos.ParDeTerminos;
import terminos.Programa;
import terminos.Solucion;
import terminos.Termino;
import terminos.Variable;

public class TestProgramaSLD {

	
	Programa programa1 = new Programa();
	Programa programa2 = new Programa();
	Programa ancestro = new Programa();
	Programa suma = new Programa();
	Programa corte = new Programa();
	Termino varX = new Variable("X");
	Termino charA = new Caracter('a');
	Termino charB = new Caracter('b');
	Termino charC = new Caracter('c');
	Termino charD = new Caracter('d');
	Termino charE = new Caracter('e');
	
	
	
	public TestProgramaSLD() throws Exception{
		
		String linea = "nieto(X,Z):-hijo(X,Y),hijo(Y,Z).";
		Clausula clausula = Parser.parseClausula(linea);
		programa1.addRegla(clausula);
		
		linea = "hijo(X,Y):-madre(Y,X).";
		clausula = Parser.parseClausula(linea);
		programa1.addRegla(clausula);
		
		linea = "hijo(X,Y):-padre(Y,X).";
		clausula = Parser.parseClausula(linea);
		programa1.addRegla(clausula);
		
		linea = "padre(a,b).";
		clausula = Parser.parseClausula(linea);
		programa1.addRegla(clausula);
		
		linea = "padre(a,b).";
		clausula = Parser.parseClausula(linea);
		programa1.addRegla(clausula);	
		
		linea = "humano(d).";
		clausula = Parser.parseClausula(linea);
		programa2.addRegla(clausula);
		
		linea = "humano(e).";
		clausula = Parser.parseClausula(linea);
		programa2.addRegla(clausula);
		
		linea = "hombre(a).";
		clausula = Parser.parseClausula(linea);
		programa2.addRegla(clausula);
		
		linea = "hombre(b).";
		clausula = Parser.parseClausula(linea);
		programa2.addRegla(clausula);
		
		linea = "hombre(c).";
		clausula = Parser.parseClausula(linea);
		programa2.addRegla(clausula);
		
		linea = "hombre(Y):-humano(Y).";
		clausula = Parser.parseClausula(linea);
		programa2.addRegla(clausula);
		
		linea = "hombre(Z):-humano(Z),noMujer(Z).";
		clausula = Parser.parseClausula(linea);
		programa2.addRegla(clausula);	

		linea = "noMujer(c).";
		clausula = Parser.parseClausula(linea);
		programa2.addRegla(clausula);
		
		linea = "noMujer(e).";
		clausula = Parser.parseClausula(linea);
		programa2.addRegla(clausula);

		linea = "hombre(Y):-noMujer(Y),!,humano(Y).";
		clausula = Parser.parseClausula(linea);
		programa2.addRegla(clausula);
		
		
		ancestro = IO.DiscoIO.leerPrograma("./ancestro.pl");
		
		suma = IO.DiscoIO.leerPrograma("./suma.pl");
		
		corte = IO.DiscoIO.leerPrograma("./corte.pl");
	}
	
	@Test
	public void testSimple() throws Exception{
		//humano(d);
		List<Termino> elem = new LinkedList<Termino>();
		elem.add(charD);
		Compuesto pregunta = new Compuesto("humano", elem );
		SLD sld = new SLD(programa2);
		assertTrue( sld.estudiarSolucion(pregunta));
	}
	

	@Test
	public void testSolucionSimpleVariasSoluciones() throws Exception{
		List<ParDeTerminos> listaPares = new LinkedList<ParDeTerminos>();
		ParDeTerminos par = new ParDeTerminos(varX, charA);
		listaPares.add(par);
		Solucion solucionEsperada = new Solucion( listaPares );
		
		SLD sld = new SLD(programa2);
		Termino pregunta = Parser.parseTermino("hombre(X)");
		boolean haySolucion = sld.estudiarSolucion( (Compuesto) pregunta );
		assertTrue( haySolucion );
		
		Solucion solucion = sld.getSolucion();
		assertTrue( solucionEsperada.equals(solucion) );
		
		haySolucion = sld.hayMasSoluciones();
		assertTrue( haySolucion );
		
		listaPares.clear();
		par = new ParDeTerminos(varX, charB);
		listaPares.add(par);
		solucionEsperada = new Solucion(listaPares);
		solucion = sld.getSolucion();
		assertTrue( solucionEsperada.equals(solucion) );
		
		haySolucion = sld.hayMasSoluciones();
		assertTrue( haySolucion );
		
		listaPares.clear();
		par = new ParDeTerminos(varX, charC);
		listaPares.add(par);
		solucionEsperada = new Solucion(listaPares);
		solucion = sld.getSolucion();
		assertTrue( solucionEsperada.equals(solucion) );
		
		haySolucion = sld.hayMasSoluciones();
		assertTrue( haySolucion );
		
		listaPares.clear();
		par = new ParDeTerminos(varX, charD);
		listaPares.add(par);
		solucionEsperada = new Solucion(listaPares);
		solucion = sld.getSolucion();
		assertTrue( solucionEsperada.equals(solucion) );
		
		haySolucion = sld.hayMasSoluciones();
		assertTrue( haySolucion );
		
		listaPares.clear();
		par = new ParDeTerminos(varX, charE);
		listaPares.add(par);
		solucionEsperada = new Solucion(listaPares);
		solucion = sld.getSolucion();
		assertTrue( solucionEsperada.equals(solucion) );
		
		haySolucion = sld.hayMasSoluciones();
		assertTrue( haySolucion );
		
		listaPares.clear();
		par = new ParDeTerminos(varX, charE);
		listaPares.add(par);
		solucionEsperada = new Solucion(listaPares);
		solucion = sld.getSolucion();
		assertTrue( solucionEsperada.equals(solucion) );
		
		haySolucion = sld.hayMasSoluciones();
		assertFalse( haySolucion );
	}

}
