package test;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import parser.Parser;
import terminos.Cadena;
import terminos.Caracter;
import terminos.Clausula;
import terminos.Compuesto;
import terminos.Corte;
import terminos.Numerico;
import terminos.Termino;
import terminos.Variable;

public class TestParser {

	Caracter charA = new Caracter('a');
    Caracter charB = new Caracter('b');
    Caracter charC = new Caracter('c');

    Variable varX = new Variable("X");
    Variable varY = new Variable("Y");
    Variable varZ = new Variable("Z");
    Variable varT = new Variable("T");
    
    Numerico num32 = new Numerico(32);
    
    Compuesto ejemplo10E;
    Compuesto ejemplo10F;
    
    Compuesto ejemplo13E;
    Compuesto ejemplo13F;
    
    Compuesto ejemplo14E;
    Compuesto ejemplo14F;

    Compuesto ejemplo15E; // p(a,X,h(g(Z)))
    Compuesto ejemplo15F; // p(Z,h(Y),h(Y))
	
	
	public TestParser() throws Exception{
		InitializeEjemplo10();
        InicializaEjemplo13();
        InicializaEjemplo14();
        InicializaEjemplo15();
	}
	
	@Test
	public void testParseVariable(){
		String cadena = "X";
		Termino variable = new Variable("X");
		Termino prueba;
		try {
			prueba = Parser.parseTermino(cadena);
			assertTrue("Parseo de una variable", variable.equals(prueba));
		} catch (Exception e) {
			fail( e.getMessage() );
		}
	}
	
	@Test
	public void testParseVariableConEspacios(){
		String cadena =" X ";
		Termino variable = new Variable("X");
		Termino prueba;
		try {
			prueba = Parser.parseTermino(cadena);
			assertTrue("Parseo de una variable con espacios", variable.equals(prueba));
		} catch (Exception e) {
			fail( e.getMessage() );
		}
		
	}

	@Test
	public void testParseVariableLarga(){
		String cadena = "XestaEsVariable";
		Termino variable = new Variable("XestaEsVariable");
		try{
			Termino prueba = Parser.parseTermino(cadena);
			assertTrue("Parseo de una variable con nombre largo", variable.equals(prueba));
		}catch( Exception e ){
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testParseVariableMinuscula(){
		String cadena = "x";
		Termino variable = new Variable("x");
		try{
			Termino prueba = Parser.parseTermino(cadena);
			assertFalse("El valor x no es tomado como variable", variable.equals(prueba));
		}catch(Exception e){
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testParseCadena(){
		String texto= "cadena";
		Termino cadena = new Cadena("cadena");
		try{
			Termino prueba = Parser.parseTermino(texto);
			assertTrue("Comprobando el parse de una cadena" , cadena.equals(prueba));
		}catch( Exception e){
			fail( e.getMessage());
		}
	}
	
	@Test
	public void testParseNumerico(){
		String texto = "32" ;
		Termino numerico = new Numerico(32);
		try{
			Termino prueba = Parser.parseTermino(texto);
			assertTrue("Comprobando el parse de un numérico" , numerico.equals(prueba));
		}catch(Exception e ){
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testParseNumericoNegativo(){
		String texto = "-32" ;
		Termino numerico = new Numerico(-32);
		try{
			Termino prueba = Parser.parseTermino(texto);
			assertTrue("Comprobando el parse de un numérico negativo" , numerico.equals(prueba));
		}catch(Exception e){
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testParseCorte() throws Exception{
		String cadena = "!";
		Termino corte = new Corte();
		assertTrue( corte.equals( Parser.parseTermino(cadena)));
	}
	
	
	@Test
	public void testParseFuncion(){
		String texto = "p(a,f(X,Y),g(h(Z)))";
		Termino funcion = ejemplo10F;
		try{
			Termino prueba = Parser.parseTermino(texto);
			assertTrue("Parseando la cadena \"p(a,f(X,Y),g(h(Z)))\"", funcion.equals(prueba));
		}catch(Exception e ){
			fail(e.getMessage());
		}
	}
		
	@Test
	public void terminoValido(){
		String texto1 = "p(a,f(X,Y),g(h(Z)))";
		String texto2 = " p( a , f( X , Y ) , g( h( Z ) ) ) ";
		String texto3 = "p(a23d , fdaf(XDF, Yxf) ,g( hadafASDF(Zsd )) )";
		String texto4 = "p(a,f(X,-12),g(h(23)))";
		String variable1 = "SDFE";
		String variable2 = " Xdaf3d ";
		String literal1 = " ads";
		String literal2 = " aDW ";
		String literal3 = " a";
		String numerico1 = "3";
		String numerico2 = " 23";
		String numerico3 = " -34 ";
		String numerico4 = " 1";
		String complejo1 = "papa(df,XD,21)";
		String complejo2 = "paDF( DD, XX , D)";
		String complejo3 = "p2( 23 , 23 , -32)";
		String complejo4 = "p3( p(3) , h , g(ds))";
		
		assertTrue( Parser.isTermino(texto1));
		assertTrue(Parser.isTermino(texto2));
		assertTrue(Parser.isTermino(texto3));
		assertTrue(Parser.isTermino(texto4));
		
		assertTrue(Parser.isTermino(variable1));
		assertTrue(Parser.isTermino(variable2));
		assertTrue(Parser.isTermino(literal1));
		assertTrue(Parser.isTermino(literal2));
		assertTrue(Parser.isTermino(literal3));
		assertTrue(Parser.isTermino(numerico1));
		assertTrue(Parser.isTermino(numerico2));
		assertTrue(Parser.isTermino(numerico3));
		assertTrue(Parser.isTermino(numerico4));
		assertTrue(Parser.isTermino(complejo1));
		assertTrue(Parser.isTermino(complejo2));
		assertTrue(Parser.isTermino(complejo3));
		assertTrue(Parser.isTermino(complejo4));
	}
	
	@Test
	public void testIsNumerico(){
		String numero1 = "21";
		String numero2 = " 34";
		String numero3 = " -34";
		
		assertTrue( "Comprobando si valida 21", Parser.isNumerico(numero1));
		assertTrue( "Comprobando si valida 34 ", Parser.isNumerico(numero2));
		assertTrue( "Comprobando si valida -34" ,Parser.isNumerico(numero3));
	}
	
	@Test
	public void testNoNumerico()
	{
		String numero1 = "23-32";
		String numero2 = "32d";
		String numero3 = "23.3";
		assertFalse( "Comprobando si 23-32 es numero" , Parser.isNumerico(numero1));
		assertFalse( "Comprobando si 32 es numero", Parser.isNumerico(numero2));
		assertFalse( "Comprobanso si 23.3 es numero ", Parser.isNumerico(numero3));
	}
		
	@Test
	public void testIsCadena(){
		String cadena1 = "estoEsCadena";
		String cadena2 = " estoTambien";
		String cadena3 = " yEst3o_o ";
		 
		assertTrue( "Compronbando si \"estoEsCadena\"" , Parser.isCadena( cadena1 ));
		assertTrue( "Comprobando si \" estoTambien\"" , Parser.isCadena(cadena2));
		assertTrue( "Comprobando si \" yEst3o_o " , Parser.isCadena(cadena3));
	}
	
	@Test
	public void testFail(){
		String fail = "!";
		assertTrue( "Comprobando si \"!\" es FAIL " , Parser.isCorte( fail ));
	}
	
	@Test
	public void testNoIsCadena(){
		String cadena1 = " a ";
		String cadena2 = " ADdd";
		String cadena3 = "3dd";
		String cadena4 = "_df";
		
		assertFalse( "Comprobando si \"a\" es cadena  ", Parser.isCadena(cadena1));
		assertFalse( "Comprobando si \" ADdd\" es cadena" , Parser.isCadena(cadena2));
		assertFalse( "Comprobando si \"3dd\" es cadena" , Parser.isCadena(cadena3));
		assertFalse( "Comprobando si \"_df\" es cadena " , Parser.isCadena(cadena4));
	}
	
	@Test 
	public void testCompuestoUnElementoSinEspacios(){
		String compuesto1 = "a(a)";
		String compuesto2 = "b(X)";
		String compuesto3 = "c(34)";
		String compuesto4 = "ds(-34)";
		
		assertTrue( "Comprobando \"a(a)\" " , Parser.isCompuesto(compuesto1));
		assertTrue( "Comprobando \"b(X)\" " , Parser.isCompuesto(compuesto2));
		assertTrue( "Comprobando \"c(34)\" ", Parser.isCompuesto(compuesto3));
		assertTrue( "Comprobando \"ds(-34)\"",Parser.isCompuesto(compuesto4));
	}
	
	@Test
	public void testCompuestoConOtroCompuesto(){
		String compuesto1 = "g(d(d))";
		
		assertTrue( "Comprobando \"g(d(d))\" ", Parser.isCompuesto(compuesto1));
	}
	
	@Test
	public void testCompuestoConMasCompuestos(){
		String compuesto1 = "g(d(d(d,98)),d,X)";
		
		assertTrue( "Comprobando \"g( d(d(d)),d,X)\" ", Parser.isCompuesto(compuesto1));
	}
	
	@Test
	public void testCompuestoConMasCompuestosComplejosYEspacios(){
		String compuesto = "p( f(a,b) , X , 32)";
		assertTrue("Comprobando \"p( f(a,b) , X , 32)\"" , Parser.isCompuesto(compuesto));
	}

	@Test
	public void testParseClausulaSimple() throws Exception{
		String st1 = "padre(a,b).";
		String st2 = "madre(b,c).";
		
		List<Termino> lista1 = new LinkedList<>();
		lista1.add(charA);
		lista1.add(charB);
		
		List<Termino> lista2 = new LinkedList<>();
		lista2.add(charB);
		lista2.add(charC);
		
		List<Termino> listaVacia = new LinkedList<>();

		Termino term1 = new Compuesto( "padre" , lista1 );
		Termino term2 = new Compuesto("madre", lista2 );
		
		Clausula cl1 = new Clausula(term1, listaVacia);
		Clausula cl2 = new Clausula(term2, listaVacia);
		
		Clausula cl1a = Parser.parseClausula(st1);
		Clausula cl2a = Parser.parseClausula(st2);
		
		assertTrue( cl1.equals(cl1a));
		assertTrue( cl2.equals(cl2a));
		
	}
	
	@Test
	public void testParseClausulaCompleja() throws Exception{
		String st1 = "nieto(X,Y):-hijo(32,Y),! , hijo(dfdf,Z).";
		
		
		List<Termino> lista1 = new LinkedList<>();
		lista1.add(varX);
		lista1.add(varY);
		Termino cabeza = new Compuesto( "nieto" , lista1 );
		
		List<Termino> lista2 = new LinkedList<>();
		lista2.add(num32);
		lista2.add(varY);
		Termino cuerpo1 = new Compuesto("hijo", lista2 );
		
		List<Termino> lista3 = new LinkedList<>();
		Termino cadena1 = new Cadena("dfdf");
		lista3.add(cadena1);
		lista3.add(varZ);
		Termino cuerpo3 = new Compuesto("hijo", lista3 );

		Termino cuerpo2 = new Corte();
		
		List<Termino> listaCuerpo = new LinkedList<>();
		listaCuerpo.add(cuerpo1);
		listaCuerpo.add(cuerpo2);
		listaCuerpo.add(cuerpo3);
		
		
		Clausula clausula = new Clausula(cabeza, listaCuerpo);
	
		Clausula cl1a = Parser.parseClausula(st1);

		assertTrue( clausula.equals(cl1a) );
	}
	
	@Test
	public void testClausulaSimpleValida(){
		String st1 = "padre(a,b).";
		String st2 = "madre(b,c).";
		String st3 = " jose( X , DF, g(2,43) , a ).";
		assertTrue(Parser.isClausula(st1));
		assertTrue( Parser.isClausula(st2));
		assertTrue( Parser.isClausula(st3));
	}
	
	@Test
	public void testClausulaComplejaValida(){
		String st1 = "nieto(X,Y):-hijo(X,Y),! ,hijo(Y,Z).";
		String st2 = "hijo(X,Y):-madre(Y ,X )  .";
		assertTrue( Parser.isClausula(st1));
		assertTrue( Parser.isClausula(st2));
	}
	
    private void InitializeEjemplo10() throws Exception{

        LinkedList<Termino> lista10a1 = new LinkedList<>();
        LinkedList<Termino> lista10a2 = new LinkedList<>();
        LinkedList<Termino> lista10a3 = new LinkedList<>();
        LinkedList<Termino> lista10b1 = new LinkedList<>();
        LinkedList<Termino> lista10b2 = new LinkedList<>();
        LinkedList<Termino> lista10b3 = new LinkedList<>();
        LinkedList<Termino> lista10b4 = new LinkedList<>();

        lista10a3.add(varX);
        Compuesto com1a = new Compuesto("g", lista10a3); // g(X)
        
        
        lista10a2.add(charB);
        lista10a2.addLast(charC);
        Compuesto com1a2 = new Compuesto( "f" , lista10a2 ); // f(b,c)
        
        lista10a1.addFirst(charA);
        lista10a1.addLast(com1a2);
        lista10a1.addLast(com1a);
        ejemplo10E = new Compuesto("p", lista10a1); // p(a,f(b,c) , g(X))
        
        
        lista10b4.add(varZ);
        Compuesto com1b = new Compuesto("h", lista10b4); // h(Z)
        
        lista10b3.add(com1b);
        Compuesto com1b2 = new Compuesto("g", lista10b3); // g(h(Z))
        
        lista10b2.add(varX);
        lista10b2.addLast(varY);
        Compuesto com1b3 = new Compuesto("f", lista10b2); // f(X,Y)
        
        lista10b1.add(charA);
        lista10b1.addLast(com1b3);
        lista10b1.addLast(com1b2);
        ejemplo10F = new Compuesto("p", lista10b1); // p(a,f(X,Y),g(h(Z)))

    }
    
    private void InicializaEjemplo13() throws Exception{
        LinkedList<Termino> listaA  = new LinkedList<>();
        listaA.add(charA);
        listaA.addLast(varX);
        ejemplo13E = new Compuesto("p", listaA); // p(a,X)
        
        LinkedList<Termino> listaB  = new LinkedList<>();
        listaB.add(varX);
        listaB.addLast(varY);
        ejemplo13F = new Compuesto("p", listaB); //p(X,Y)
    }
    
    private void InicializaEjemplo14() throws Exception{
        
        LinkedList<Termino> listaE1 = new LinkedList<>();
        listaE1.add(varX);
        listaE1.addLast(charB);
        Compuesto E1 = new Compuesto("f", listaE1);
        
        LinkedList<Termino> listaE = new LinkedList<>();
        listaE.add(charA);
        listaE.add(E1);
        listaE.add(varY);
        
        ejemplo14E = new Compuesto("p", listaE);
        
        LinkedList<Termino> listaF1 = new LinkedList<>();
        listaF1.add(varY);
        Compuesto F1 = new Compuesto("g", listaF1);
        LinkedList<Termino> listaF2 = new LinkedList<>();
        listaF2.add(F1);
        listaF2.add(varZ);
        Compuesto F2 = new Compuesto("f", listaF2);
        
        LinkedList<Termino> listaF = new LinkedList<>();
        listaF.add(varX);
        listaF.add(F2);
        listaF.add(varT);
        
        ejemplo14F = new Compuesto("p", listaF);
    }
    
    private void InicializaEjemplo15() throws Exception{
        LinkedList<Termino> lista1 = new LinkedList<>();
        lista1.add(varZ);
        Compuesto E1 = new Compuesto("g",lista1);
        
        LinkedList<Termino> lista2 = new LinkedList<>();
        lista2.add(E1);
        Compuesto E2 = new Compuesto("h",lista2);
        
        LinkedList<Termino> listaE = new LinkedList<>();
        listaE.add(charA);
        listaE.add(varX);
        listaE.add(E2);
        ejemplo15E = new Compuesto( "p", listaE); // p(a,X,h(g(Z)))
        
        LinkedList<Termino> lista3 = new LinkedList<>();
        lista3.add(varY);
        Compuesto F1 = new Compuesto("h", lista3);
        LinkedList<Termino> lista4 = new LinkedList<>();
        lista4.add(varY);
        Compuesto F2 = new Compuesto("h", lista4);
        
        LinkedList<Termino> listaF = new LinkedList<>();
        listaF.add(varZ);
        listaF.add(F1);
        listaF.add(F2);
        ejemplo15F = new Compuesto("p", listaF);
    }
	
}
