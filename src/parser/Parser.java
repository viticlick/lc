package parser;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import terminos.Cadena;
import terminos.Caracter;
import terminos.Clausula;
import terminos.Compuesto;
import terminos.Corte;
import terminos.Fail;
import terminos.Numerico;
import terminos.Termino;
import terminos.Variable;

public class Parser {

	private static final String NUMERO = "-?[0-9]+";
	private static final String CADENA = "[a-z][a-zA-Z0-9_]+";
	private static final String CHAR = "[a-z]";
	private static final String VARIABLE = "[A-Z][a-zA-Z0-9_]*";
	private static final String DESCRIPTOR = "[a-z][a-zA-Z0-9_]*";
	private static final String CONTENIDOFUNCION = "([a-zA-Z0-9_,-]|[!]|[ ]|[(]|[)])+";
	private static final String COMPUESTO = DESCRIPTOR+"[(]"+CONTENIDOFUNCION+"[)]";
	private static final String ESPACIOS = "[ ]*";
	private static final Pattern patronNumerico = Pattern.compile(NUMERO);
	private static final Pattern patronCadena = Pattern.compile(CADENA);
	private static final Pattern patronChar = Pattern.compile(CHAR);
	private static final Pattern patronVariable = Pattern.compile(VARIABLE);
	private static final Pattern patronCompuesto = Pattern.compile(COMPUESTO);
	private static final Pattern patronClausulaSimple = Pattern.compile(COMPUESTO+"[ ]*[.]");
	private static final Pattern patronClausulaCompleja = 
			Pattern.compile(
					COMPUESTO
					+ESPACIOS
					+":-"
					+ESPACIOS 
					+ "(" + COMPUESTO+ ")*"
					+ "[ ]*[.]");
	
	public static Termino parseTermino(String cadena) throws Exception {
		Termino term = null;
		if( isCorte( cadena )){
			term = parseFail( cadena );
		}else if( isVariable( cadena ) ){
			term = parseVariable( cadena );
		}else if( isFail(cadena) ){
			term = new Fail();
		}else if( isCadena(cadena)){
			term = parserCadena(cadena);
		}else if ( isChar(cadena)){
			term = parseChar(cadena);
		}else if ( isNumerico(cadena)){
			term = parseNumerico(cadena);
		}else if( isCompuesto(cadena)){
			term = parseCompuesto( cadena );
		}else{
			throw new Exception( "La cadena \""+ cadena + "\" no es un término válido");
		}
		
		return term;
	}


	public static boolean isFail(String cadena) {	
		return cadena.equals("fail");
	}


	private static Termino parseFail(String cadena) throws Exception {
		if( !cadena.trim().equals("!")){
			throw new Exception( "No se corresponde con fail");
		}
		return new Corte();
	}


	public static boolean isTermino(String termino) {
		boolean isTermino;
		isTermino = (
				isNumerico(termino)
				|| isCadena(termino)
				|| isChar(termino)
				|| isVariable(termino)
				|| isCompuesto(termino)
				);
		return isTermino;
	}

	public static boolean isNumerico(String valor) {
		String dato = valor.trim();
		Matcher matcher =  patronNumerico.matcher(dato);
		return matcher.matches();
	}

	public static boolean isCadena(String valor) {
		String dato = valor.trim();
		Matcher matcher = patronCadena.matcher(dato);
		return matcher.matches();
	}
	
	public static boolean isChar( String character ){
		String dato = character.trim();
		Matcher matcher = patronChar.matcher(dato);
		return matcher.matches();
	}
	
	public static boolean isVariable( String valor ){
		String dato = valor.trim();
		Matcher matcher = patronVariable.matcher(dato);
		return matcher.matches();
	}

	public static boolean isCompuesto( String valor ){
		boolean matches = false;
		String dato = valor.trim();
		Matcher matcher = patronCompuesto.matcher(dato);
		if( matcher.matches() ){
			int posicionPrimerParentesis = dato.indexOf("(") ;
			int posicionUltimoParentesis = dato.lastIndexOf(")");
			String subCadena = dato.substring(posicionPrimerParentesis + 1 , posicionUltimoParentesis);
			matches = isContenidoFuncionValido(subCadena);
		}
		return matches;
	}
	
	public static Clausula parseClausula(String cadena) throws Exception {
		String dato = cadena.trim();
		Termino cabeza;
		List<Termino> cuerpo;
		int indicePunto = cadena.indexOf(".");
		if( Parser.isClausulaSimple(dato)){
			String sCabeza = cadena.substring(0,indicePunto);
			cabeza = Parser.parseCompuesto(sCabeza);
			cuerpo = new LinkedList<Termino>();
		}else if( Parser.isClausulaCompleja(dato)){
			int indicePuntosGuion = cadena.indexOf(":-");
			String sCabeza = cadena.substring(0, indicePuntosGuion);
			String sCuerpo = cadena.substring(indicePuntosGuion + 2 , indicePunto); 
			cabeza = Parser.parseCompuesto(sCabeza);
			cuerpo = Parser.parseCuerpoClausula( sCuerpo );
		}else{
			return null;
		}
		Clausula cl = new Clausula(cabeza, cuerpo);	
		return cl;
	}
	
	private static List<Termino> parseCuerpoClausula(String cuerpo) throws Exception {
		List<Termino> lista = new LinkedList<>();
		cuerpo = cuerpo.trim();
		int indiceComa = cuerpo.indexOf(",");
		while( indiceComa != -1 ){
			String subcadena = cuerpo.substring(0 , indiceComa);
			if( isCompuesto(subcadena) ){
				Termino term = Parser.parseCompuesto(subcadena);
				lista.add(term);
				cuerpo = cuerpo.substring(indiceComa + 1);
				indiceComa = cuerpo.indexOf(",");
			}else if( isCorte(subcadena)){
				Termino term = Parser.parseFail(subcadena);
				lista.add(term);
				cuerpo = cuerpo.substring(indiceComa +1 );
				indiceComa = cuerpo.indexOf(",");
			}else{
				indiceComa = cuerpo.indexOf("," , indiceComa+1 );
			}
		}
		Termino term = Parser.parseCompuesto(cuerpo);
		lista.add(term);
		return lista;
	}


	private static boolean isContenidoFuncionValido( String cadena ){
		boolean matches = false;
		
		int indice = cadena.indexOf(",");
		while( indice != -1 ){
			String subcadena = cadena.substring(0,indice);
			if( isTermino(subcadena)){
				cadena = cadena.substring( indice +1 );
				indice = cadena.indexOf(",");
			}else{
				indice = cadena.indexOf(",",indice+1);
			}
		}
		matches = isTermino(cadena);
	
		return matches;
	}
	
	public static boolean isClausula(String cadena) {
		boolean matches = false;
		if( isClausulaSimple(cadena)
				|| isClausulaCompleja(cadena)){
			matches = true;
			
		}
		return matches;
	}
	
	private static boolean isClausulaSimple(String cadena) {
		boolean matches = false;
		String dato = cadena.trim();
		Matcher matcher = patronClausulaSimple.matcher(dato);
		if( matcher.matches() ){
			int posicionPunto = cadena.indexOf(".");
			cadena = cadena.substring(0 , posicionPunto);
			matches = isCompuesto(cadena);
		}
		return matches;
	}
	
	private static boolean isClausulaCompleja(String cadena) {
		boolean matches = false;
		String dato = cadena.trim();
		Matcher matcher = patronClausulaCompleja.matcher(dato);
		if( matcher.matches() ){
			int posicionPutosGuion = dato.indexOf(":-");
			int posicionPuntoFinal = dato.indexOf(".");
			String cabeza = dato.substring(0 , posicionPutosGuion);
			String cuerpo = dato.substring(posicionPutosGuion + 2, posicionPuntoFinal);
			if( isCompuesto(cabeza)
					&& isCuerpoClausula( cuerpo )){
				matches = true;
			}
		}
		return matches;
	}

	private static boolean isCuerpoClausula(String cuerpo) {
		boolean matches = false;
		int indice = cuerpo.indexOf(",");
		while( indice != -1 ){
			String subcadena = cuerpo.substring(0, indice);
			if( isCompuesto(subcadena)
					|| isCorte(subcadena)){
				cuerpo = cuerpo.substring(indice+1);
				indice = cuerpo.indexOf(",");
			}else{
				indice = cuerpo.indexOf(",", indice+1);
			}
		}
		matches = isCompuesto(cuerpo);
		return matches;
	}

	private static Termino parseVariable(String variable){
		variable = variable.trim();
		Termino term = new Variable(variable);
		return term;
	}
	
	private static Termino parserCadena( String cadena ){
		cadena = cadena.trim();
		Termino term = new Cadena(cadena);
		return term;
	}
	
	private static Termino parseChar( String character ){
		character = character.trim();
		Termino term = new Caracter(character.charAt(0));
		return term;
	}
	
	private static Termino parseNumerico( String numero ){
		numero = numero.trim();
		Termino term = new Numerico( Double.valueOf(numero) );
		return term;
	}

	private static Termino parseCompuesto(String cadena) throws Exception {
		cadena = cadena.trim();
		int primerParentesis = cadena.indexOf("(");
		int ultimoParentesis = cadena.lastIndexOf(")");
		String nombreFuncion = cadena.substring(0, primerParentesis);
		String contenidoFuncion = cadena.substring(primerParentesis+1 , ultimoParentesis);
		List<Termino> terminos = getListTerm(contenidoFuncion);
		Termino compuesto = new Compuesto(nombreFuncion, terminos);
		return compuesto;
	}
	
	private static List<Termino> getListTerm( String cadena ) throws Exception
	{
		List<Termino> lista = new LinkedList<Termino>();
		int indice = cadena.indexOf(",");
		while( indice != -1 ){
			String subcadena = cadena.substring(0 , indice);
			if( isTermino(subcadena)){
				Termino termino = Parser.parseTermino(subcadena);
				lista.add(termino);
				cadena = cadena.substring(indice+1);
				indice = cadena.indexOf(",");
			}else{
				indice = cadena.indexOf(",",indice+1);
			}
		}
		Termino termino = Parser.parseTermino(cadena);
		lista.add(termino);
		
		return lista;
	}


	public static boolean isCorte(String fail) {
		boolean toret = false;
		String cadena = fail.trim();
		if( cadena.equals("!")){
			toret = true;
		}
		return toret;
	}


}
