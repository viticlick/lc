package algoritmia;

public class FailException extends Exception {

	public FailException(){
		super("Fail");
	}
	
	public FailException( String msg ){
		super( msg );
	}
	
	private static final long serialVersionUID = 1L;
	
	
	
}
