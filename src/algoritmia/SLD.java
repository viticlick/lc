package algoritmia;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import terminos.Clausula;
import terminos.Compuesto;
import terminos.ListaVariables;
import terminos.ParDeTerminos;
import terminos.Programa;
import terminos.Solucion;
import terminos.Termino;
import terminos.TipoTermino;
import terminos.Variable;

public class SLD {

	private Programa programa;
	private Estado estadoActual;
	private static LinkedList<Estado> listaEstados = new LinkedList<>();
	private static LinkedList<Compuesto> listaPreguntasExpandidas = new LinkedList<>();
	private static int profunidadActual = 0;
	MemoriaEstados memoriaEstados = MemoriaEstados.getInstancia();
	
	public SLD ( Programa programa ){
		this.programa = programa;
	}
	
	public SLD( List<Clausula> programa ){
		this.programa = new Programa( programa );
	}
	
	public boolean estudiarSolucion( Compuesto pregunta ) throws Exception {
		listaEstados.clear();
		iniciarEstudio(pregunta);
		estudio();
		return estadoActual.solucionado;
	}
	
	protected boolean  subEstudiarSolucion( Compuesto pregunta ) throws Exception{
		iniciarEstudio( pregunta );
		estudio();
		return estadoActual.solucionado;
	}
	
	public Solucion getSolucion(){
		return estadoActual.solucion;
	}
	
	public boolean hayMasSoluciones() {
		try{
			estudiarListaSoluciones();
		}catch( FailException ex ){
			return false;
		}
		return estadoActual.solucionado;
	}
	
	private void iniciarEstudio(Compuesto pregunta) throws FailException {
		estadoActual = new Estado();
		estadoActual.pregunta = pregunta;
		estadoActual.reglaEstudio = new Clausula( pregunta,  new LinkedList<Termino>() );
		estadoActual.Profundidad = profunidadActual;
		estadoActual.Expandido = preguntaYaEstudiada( pregunta );
	}
	
	private boolean preguntaYaEstudiada(Compuesto pregunta) {
		boolean preguntaEstudiada = false;
		Iterator<Compuesto> iterador = listaPreguntasExpandidas.iterator();
		while( iterador.hasNext() 
				&& !preguntaEstudiada){
			Compuesto com = iterador.next();
			preguntaEstudiada = com.equals(pregunta);
		}
		return preguntaEstudiada;
	}

	private void estudio() throws Exception {
		
		if( !finUnificacion( estadoActual) )
		{
			if( ! estadoActual.Expandido ){
				Iterator<Clausula> iterador = programa.iterator();
				Stack<Estado> pilaEstados = new Stack<>();
				while( iterador.hasNext() ){
					Clausula regla = iterador.next();
					Clausula reglaEstudio = generaNuevaRegla(regla);
					// TODO generar nuevas reglas y añadirlas al array
					Termino cabeza = reglaEstudio.getCabeza();
					try{
						Solucion solucion = Robinson.getUnificador( estadoActual.pregunta, cabeza);
						for( ParDeTerminos par : solucion.getPares() )
						{
							reglaEstudio = reglaEstudio.sustituir(par.getVariable(), par.getTermino());
						}
						Estado nuevoEstado = new Estado();
						nuevoEstado.reglaEstudio = reglaEstudio;
						nuevoEstado.solucion = solucion;
						nuevoEstado.Profundidad = estadoActual.Profundidad + 1 ;
						pilaEstados.push(nuevoEstado);
						
						
					}catch( FailException ex){
						// No unifican
					} 
				}
				
				listaPreguntasExpandidas.add( estadoActual.pregunta );
				if( pilaEstados.isEmpty() ){
					throw new FailException();
				}
				
				while( !pilaEstados.isEmpty() )
					listaEstados.addFirst( pilaEstados.pop() );
				
			}else{
				try{
					// Este caso se da cuando el siguiente estado a estudiar es una regla 
					// que se encuentra a la misma profundidad, de ser el caso o no existir más reglas
					// devolvería Fail, ya que no tiene solución el algoritmo
					if( listaEstados.getFirst().Profundidad == estadoActual.Profundidad ){
						throw new FailException();
					}
				}catch( Exception ex){
					throw new FailException();
				}
			}
			estudiarListaSoluciones();	
		}
	}

	private void estudiarListaSoluciones() throws FailException {
		boolean solucionado = false;
		while( ! solucionado
				&& ! listaEstados.isEmpty() ){
			
			try{
				estadoActual = listaEstados.pollFirst();
				if( ! estadoActual.reglaEstudio.hasElementos() ){ 
					solucionado = finUnificacion(estadoActual);
				}else{
					solucionado = estudioComplejo();
				}
			}catch(FailException fail ){
				// Seguir estudio
				solucionado = false;
			}
		}
		if( ! solucionado ){
			throw new FailException();
		}
	}

	private boolean estudioComplejo() throws FailException {
		// TODO revisar las subreglas
		boolean solucionadoEstado = false;
		int numSubReglas = estadoActual.reglaEstudio.getCuerpo().size();
		int indiceSubRegla;
		for( indiceSubRegla = 0 ; indiceSubRegla < numSubReglas ; indiceSubRegla ++ ){
			
			Termino termino = estadoActual.reglaEstudio.getCuerpo().get(indiceSubRegla);
			if( termino.getTipo() == TipoTermino.CORTE ){
				// TODO repasar limpieza de estados!!
				listaEstados.clear();
			}else if(termino.getTipo() == TipoTermino.FAIL){
				throw new FailException();
			}else if( termino.getTipo() == TipoTermino.COMPUESTO){			
				solucionadoEstado = solucionSubTermino(termino);
			}
		}
		return solucionadoEstado;
	}

	private boolean solucionSubTermino(Termino termino) throws FailException {
		boolean solucionado;
		boolean solucionParcial = false;
		profunidadActual++;
		try {
			SLD subConsulta = new SLD(programa);
			
			solucionParcial =  subConsulta.subEstudiarSolucion( (Compuesto) termino );
			if( solucionParcial ){
				// TODO hacer transformaciones de la regla o seguir con su estudio
				Estado salvarEstado = estadoActual.clone();
				salvarEstado.Expandido = true;
				listaEstados.addFirst( salvarEstado );
				Solucion subSolucion = subConsulta.getSolucion();
				estadoActual.solucion = estadoActual.solucion.aplicaTransitividad(subSolucion);
				estadoActual.solucionado = true;
				for( ParDeTerminos par : subSolucion.getPares() )
				{
					estadoActual.reglaEstudio = estadoActual.reglaEstudio.sustituir(par.getVariable(), par.getTermino());
				}
			}else{
				throw new FailException();
			}
			
		} catch (FailException fail) {
			throw fail;
		} catch (Exception e){
			e.printStackTrace();
		}finally{
			profunidadActual--;
		}
		
		solucionado = solucionParcial;
		return solucionado;
	}
	
	private Clausula generaNuevaRegla( Clausula regla ) throws Exception{
		Clausula nuevaRegla = regla.clone();
		ListaVariables listaVariables = nuevaRegla.getListaVariables();
		List<Variable> listaNVariables = listaVariables.getLista();
		for (Variable variable : listaNVariables) {
			Variable nuevaVariable = memoriaEstados.getNuevaVariable();
			// TODO refactorizar esto, y crear una funcion que evite crear una regla por cada sustitución, haciendolas todas juntas
			nuevaRegla = nuevaRegla.sustituir( variable , nuevaVariable );
		}
		return nuevaRegla;
	}
	
	private boolean finUnificacion( Estado estado ){
		Clausula clausula = estado.reglaEstudio;
		boolean finUnificacion = false;
		if( ! clausula.hasVariables() ){
			Iterator<Clausula> iterador = programa.iterator();
			while( !finUnificacion 
					&& iterador.hasNext() ){
				Clausula cl = iterador.next();
				if( ! cl.hasElementos() ){
					try{
						Solucion solucion = Robinson.getUnificador(cl.getCabeza(), clausula.getCabeza());
						estado.solucion = estado.solucion.aplicaTransitividad(solucion);
						estado.solucionado = true;
						finUnificacion = true;
					}catch(FailException ex){
						// No unifican
					}catch(Exception ex){
						// Error en la transitividad
					}
				}
			}
		}
		return finUnificacion;
	}

	

}
