/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package algoritmia;

import java.util.LinkedList;
import java.util.Stack;

import terminos.Compuesto;
import terminos.ParDeTerminos;
import terminos.Solucion;
import terminos.Termino;
import terminos.TipoTermino;

/**
 *
 * @author vlopez
 */
public class Robinson {
    
	/**
	 * Busca los pares que se han de sustituir para conseguir el unificador de máxima generalidad de los dos términos. 
	 * En caso de que los dos términos sean variables, se tomará como variable el primer termino y como valor el segundo.
	 * @param terminoEntrada1 Termino a analizar
	 * @param terminoEntrada2 Termino a analizar
	 * @return una solución que contiene los cambios necesarios para llegar al match de las variables
	 * @throws Exception Se lanza cuando no es posible hacer match de los dos términos introducidos
	 */
    public static Solucion getUnificador( Termino terminoEntrada1 , Termino terminoEntrada2 ) throws FailException{
        
        LinkedList<ParDeTerminos> listaMatch = new LinkedList<>();
        Stack<ParDeTerminos> pila = new Stack<>();
        ParDeTerminos par = new ParDeTerminos(terminoEntrada1, terminoEntrada2);
        pila.push(par);
        
        // Mientra haya dos Terminos a comparar 
        while( !pila.empty() ){
            par = pila.pop();
            Termino termA = par.getPrimero();
            Termino termB = par.getSegundo();
            
            // Comprueba si los t��rminos son iguales
            if( !termA.equals(termB) ){
                ParDeTerminos discordancia = termA.getDiscordancia(termB);
                
                // Comprueba si uno de los dos terminos tiene variables
                if( discordancia != null
                		&& discordancia.hasVariables()){
                    Termino variable = discordancia.getVariable();
                    Termino termino = discordancia.getTermino();
                    
                    // Comprueba el occur-check
                    boolean occurence = occurCheck(termino, variable);
                    if( ! occurence ){
                        
                        // Construye la soluci��n
                        listaMatch.addLast(new ParDeTerminos(variable, termino));
                        try{
	                        Termino term1Sustituido = termA.sustituir(variable, termino);
	                        Termino term2Sustituido = termB.sustituir(variable, termino);
	                        pila.push(new ParDeTerminos(term1Sustituido, term2Sustituido));
                        }catch(Exception e){
                        	throw new FailException("Error inexperado");
                        }
                    }else{
                        // OccurCheck, No unifican, fin del proceso.
                    	throw new FailException("OccurCheck");
                    }
                }else{
                    throw new FailException();
                }
            }
        }
        Solucion solucion = new Solucion(listaMatch);
        return solucion;
    }

    public static boolean unifican( Termino term1 ,  Termino term2 ){
    	boolean unifican = false;
    	try{
    		getUnificador(term1, term2);
    		unifican = true;
    	}catch(Exception ex){
    		
    	}
    	return unifican;
    }
    
    private static boolean occurCheck(Termino termino, Termino variable) {
        boolean occurence = false;
        if( termino.getTipo() == TipoTermino.COMPUESTO ){
            occurence = ((Compuesto) termino).contieneTermino(variable);
        }
        return occurence;
    }
}
