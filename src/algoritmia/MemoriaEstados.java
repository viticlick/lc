package algoritmia;

import java.util.LinkedList;
import java.util.List;

import terminos.Clausula;
import terminos.ListaVariables;
import terminos.Variable;

public class MemoriaEstados extends LinkedList<Estado>{
	
	private static final long serialVersionUID = -6689575046156602659L;
	private static MemoriaEstados INSTANCIA = new MemoriaEstados();
	private static int contador=0;
	
	private MemoriaEstados(){
		super();
	}
	
	public static MemoriaEstados getInstancia(){
		return INSTANCIA;
	}
	
	public static Variable getNuevaVariable(){
		contador++;
		Variable variable = new Variable("G_"+contador+"*");
		return variable;
	}
	
	public static Clausula generaNuevaRegla( Clausula regla ) throws FailException, Exception{
		Clausula nuevaRegla = regla.clone();
		ListaVariables listaVariables = nuevaRegla.getListaVariables();
		List<Variable> listaNVariables = listaVariables.getLista();
		for (Variable variable : listaNVariables) {
			Variable nuevaVariable = getNuevaVariable();
			nuevaRegla = nuevaRegla.sustituir( variable , nuevaVariable );
		}
		return nuevaRegla;
	}
	
}
