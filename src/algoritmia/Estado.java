package algoritmia;

import terminos.Clausula;
import terminos.Compuesto;
import terminos.Solucion;

public class Estado {
	
	public Clausula reglaEstudio = null;
	public Solucion solucion = null;
	public Compuesto pregunta = null;
	public int Profundidad = 0;
	public boolean subProgramaSolucionado = false;
	public boolean solucionado = false;
	public boolean Expandido = false;
	
	public Estado(){
		
	}
	
	public Estado clone(){
		Estado nuevoEstado = new Estado();
		nuevoEstado.reglaEstudio = this.reglaEstudio.clone();
		nuevoEstado.solucion = this.solucion.clone();
		nuevoEstado.pregunta = this.pregunta ;
	    nuevoEstado.subProgramaSolucionado = this.subProgramaSolucionado;
		nuevoEstado.solucionado = this.solucionado;
		nuevoEstado.Expandido = this.Expandido;
		nuevoEstado.Profundidad = this.Profundidad;
		return nuevoEstado;
	}
	
}
