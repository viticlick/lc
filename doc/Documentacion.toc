\select@language {spanish}
\contentsline {section}{\numberline {1}Descripci\IeC {\'o}n breve de la pr\IeC {\'a}ctica}{3}
\contentsline {section}{\numberline {2}Descripci\IeC {\'o}n detallada del lenguaje de interfaz soportado}{3}
\contentsline {section}{\numberline {3}Descripci\IeC {\'o}n de la implementaci\IeC {\'o}n realizada}{3}
\contentsline {subsection}{\numberline {3.1}Simplificaciones realizadas}{3}
\contentsline {subsection}{\numberline {3.2}Estructura del c\IeC {\'o}digo fuente}{3}
\contentsline {subsection}{\numberline {3.3}Descripci\IeC {\'o}n de las estructuras de datos utilizadas}{3}
\contentsline {subsection}{\numberline {3.4}Funciones o m\IeC {\'e}todos m\IeC {\'a}s relevantes}{3}
\contentsline {section}{\numberline {4}Instrucciones de compilaci\IeC {\'o}n y uso}{5}
\contentsline {section}{\numberline {5}Casos de prueba documentados}{6}
\contentsline {section}{\numberline {6}Conclusiones}{6}
\contentsline {section}{\numberline {7}Bibliograf\IeC {\'\i }a}{6}
